var path    = require('path');
var Text    = require('extract-text-webpack-plugin');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: [
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/dev-server',
    './app/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: './app/'
  },
  resolve: {
    root: [path.resolve(__dirname, 'app')],
    extensions: ['', '.js', 'jsx']
  },
  plugins: [
    new Text('styles.css', { allChunks: true }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.ProvidePlugin({
      'React':     'react',
      '_':         'lodash',
      'Promise':   'bluebird',
      'ReactDOM':  'react-dom',
      'cssModule': 'react-css-modules',
      'TestUtils': 'react-addons-test-utils'
    })
  ],
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        loaders: ['react-hot', 'babel'],
        include: path.join(__dirname, 'app')
      },
      {
        test: /\.css$/,
        loader: Text.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss-loader')
      }
    ]
  },
  postcss: [ require('autoprefixer'), ],
};
