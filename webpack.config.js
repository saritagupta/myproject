var config = {
   entry: './app/index.js',

   output: {
      path:'./',
      filename: './app/bundle.js',
   },

   devServer: {
      inline: true,
      port: 8080
   },

   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel',

            query: {
               presets: ['es2015', 'react']
            }
        },
        {
          test: /\.css$/, // Transform all .css files required somewhere within an entry point...
          loaders: ['css-loader'] // ...with PostCSS
        }
      ]
   }
}

module.exports = config;
