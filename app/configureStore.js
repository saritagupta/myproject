import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { reduxReactRouter, routerStateReducer, ReduxRouter } from 'redux-router';
import { createHistory } from 'history';
import thunk from 'redux-thunk';
import * as reducers from './Reducers/index';
import _ from 'lodash';

let combinedCreateStore
const storeEnhancers = [
    reduxReactRouter({
        createHistory
    }),
    window.devToolsExtension ? window.devToolsExtension() : f => f
]

combinedCreateStore = compose(...storeEnhancers)(createStore)
const finalCreateStore = applyMiddleware(thunk)(combinedCreateStore)
const combinedReducer = combineReducers(_.assign({
  router: routerStateReducer
}, reducers))

export default function configureStore(initialState) {
  return finalCreateStore(combinedReducer, initialState);
}
