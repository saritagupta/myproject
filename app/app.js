import React,{Component, PropTypes} from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { connect } from 'react-redux'
import { select } from './Selectors/default-selector';
import { bindActionCreators } from 'redux';
import { getActionCreators } from './Actions/action-creator';
import Header from './Common/Components/Header/header';
import Footer from './Common/Components/Footer/footer';
import AuthenticatedComponent from "./Common/Components/AuthenticatedComponent/authenticated-component";
import _ from 'lodash';
import * as DefaultActions from './Actions/defaultActions';
let hoc = AuthenticatedComponent(class App extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let childrenWithProps = React.Children.map(this.props.children, (child) => {
             return React.cloneElement(child, { });
        });
        return (
            <div>
              <Header navigate={this.props.actions.navigate} loggedIn={this.props.isAuthenticated} />
                  <div className="container content-wrapper"  fluid={true}>
                      {childrenWithProps}
                  </div>
              <Footer navigate={this.props.actions.navigate} />
            </div>
      );
    }

});

export default connect(select,  (dispatch) => ({actions: bindActionCreators(getActionCreators(DefaultActions), dispatch)}))(hoc);
