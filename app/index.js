import React from 'react';
import ReactDOM from 'react-dom';
import { getRoutes } from './routes';
import Immutable from 'immutable';
import { Provider } from 'react-redux';
import configureStore from './configureStore';
import { Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';
import { polyfill as promisePolyfill } from 'es6-promise';
promisePolyfill();
const store = configureStore();
 ReactDOM.render(
         <div>
            <Provider store={store}>
                { getRoutes(store) }
            </Provider>
        </div>, document.getElementById('app')
    );
