import { getUrl, fetchJSON } from '../Common/Utils/http';
import _ from 'lodash';

function getLineReport(searchCriteria, dispatch) {
    const url = getUrl('lineChart');
    return fetchJSON(url, 'GET', _.omitBy({
        }, _.isUndefined), dispatch);
};
function getPieReport(searchCriteria, dispatch) {
    const url = getUrl('pieChart');
    return fetchJSON(url, 'GET', _.omitBy({
        }, _.isUndefined), dispatch);
};
function getListData(searchCriteria, dispatch) {
    const url = getUrl('listData');
    return fetchJSON(url, 'GET', _.omitBy({
        }, _.isUndefined), dispatch);
};

var resolvedLineReport =  getLineReport;
var resolvedPieReport =  getPieReport;
var resolvedListReport =  getListData;
export {
    resolvedLineReport as  getLineReport,
    resolvedPieReport as  getPieReport,
    resolvedListReport as  getListData
};
