import { getUrl, fetchJSON } from '../Common/Utils/http';
import SessionStorage from './../Common/Utils//session-storage';
import _ from 'lodash';
import uuid from 'node-uuid';


function getUserSession() {
    return {
        AuthToken: SessionStorage.get('AuthToken'),
        userUniqueId: SessionStorage.get('userUniqueId'),
        activityId: SessionStorage.get('activityId')
    }
}

function isAuthenticated() {
    const authenticationToken = SessionStorage.get('AuthToken');
    return !_.isNil(authenticationToken) && authenticationToken !== 'null';
}

function signIn(username, password, dispatch) {
    const url = getUrl('login');
    const credentials = window.btoa(window.btoa(username) + ':' + window.btoa(password));
    return fetchJSON(url, 'GET', JSON.stringify(credentials)).then((response) => {
        let passwordExpirationDays = 0;
        if (response.LoginResponse.Success) {
            SessionStorage.set('AuthToken', response.AuthToken);
            SessionStorage.set('userUniqueId', response.UserUniqueId);
            SessionStorage.set('activityId', uuid.v1());
        }
        return {...response};
    }, dispatch);
}

let resolvedGetUserSession = getUserSession;
let resolvedIsAuthenticated = isAuthenticated;
let resolvedSignIn =  signIn;

export {
    resolvedGetUserSession as getUserSession,
    resolvedIsAuthenticated as isAuthenticated,
    resolvedSignIn as signIn
}
