import { getUrl, fetchJSON } from '../Common/Utils/http';
import _ from 'lodash';

function register(formData, dispatch) {
    const url = getUrl('register');
    return fetchJSON(url, 'GET', _.omitBy({
        formData
   }, _.isUndefined), dispatch);
};
var resolvedRegisterService =  register;
export {
    resolvedRegisterService as  register
};
