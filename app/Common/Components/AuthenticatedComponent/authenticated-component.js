import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { select } from './../../../Selectors/default-selector';
import { getActionCreators } from './../../../Actions/action-creator';
import * as DefaultActions from './../../../Actions/defaultActions';

@connect(select, (dispatch) => ({actions: bindActionCreators(getActionCreators(DefaultActions), dispatch)}))
export default (ComposedComponent) => {
    return class AuthenticatedComponent extends React.Component {
        componentWillMount() {
            this._isAuthenticated();
        }
        componentWillReceiveProps(nextProps) {
            this._isAuthenticated();
        }
        render() {
            var renderComposedComponent = this._isAuthenticated()
                    ? <ComposedComponent {...this.props}/>
                    : null
                    return (renderComposedComponent)
        }
        _isAuthenticated() {
            const routes = ['/login','/register'];
            if (_.isEmpty(this.props.router.location.pathname) || _.includes(routes, this.props.router.location.pathname.toLowerCase())) return true;
            if (!this.props.isAuthenticated) {
                this.props.actions.navigate('Login');
            }
            else {
                return true;
            }
        }
    }
};
//export default connect(select,  (dispatch) => ({actions: bindActionCreators(getActionCreators(DefaultActions), dispatch)}))(HOC);
