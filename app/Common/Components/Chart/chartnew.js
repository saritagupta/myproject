import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { VictoryPie } from "victory";
import { Panel} from 'react-bootstrap';
class ChartClassPie extends React.Component {

   render() {
      let height=250;
      return (
          <div className="col-sm-12">
              <Panel  >
              <div className="col-sm-12">
                <VictoryPie
                    height={height}
                    innerRadius={this.props.innerRadius ? 50 : 0}
                     data={this.props.data}
                     colorScale={[
                       "#D85F49",
                       "#F66D3B",
                       "#D92E1D",
                       "#D73C4C",
                       "#FFAF59",
                       "#E28300",
                       "#F6A57F"
                     ]}
                    />
                 </div>
              </Panel>
          </div>
      );
   }
}
export default ChartClassPie;
