import ChartVbar from './chartVbar';
//import Panel from "./../../../app/Common/Components/Panel/panel";
const {
  renderIntoDocument,
  findRenderedDOMComponentWithTag,
  Simulate
} = TestUtils;

const mockFunction = () => {};
const props = {
  Data:  {
      "0": [{
          "x": "apples",
          "y": " 3"
      }, {
          "x": "bananas",
          "y": "1"
      }, {
          "x": "oranges",
          "y": " 1"
      }]},
  labels: [{
      "fuji": "apples"
  }, {
      "bananas": "bananas"
  }, {
      "navel": "oranges"
  }]
}

describe('ChartVBar: ChartVBar Element Component', () => {

  it('should take ChartVBar to override default properties', () => {
    const component = renderIntoDocument(
      <ChartVbar {...props}  />
    );
    expect(component.props.Data).to.equal(props.Data);
    expect(component.props.labels).to.equal(props.labels);
  });
});
