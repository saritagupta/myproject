import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';

import { VictoryChart,VictoryStack,VictoryBar } from "victory";
import { Panel} from 'react-bootstrap';
export default class ChartClass extends React.Component {
    getDataStack(){
        return _.map(this.props.Data,(val,index)=>{
            return <VictoryBar data={val} key={index}/>
        });
    }
   render() {
        let labelArr=_.map(this.props.labels,(val)=>{
            return _.get(_.toArray(val),0,'')
        });
        let height=250;
              return (
                  <div className="col-sm-12">
                      <Panel  >
                      <div className="col-sm-12">
                            <VictoryChart
                                height={280}
                                domainPadding={{x: 100}}>
                                        <VictoryStack
                                          labels={labelArr}
                                          colorScale={"qualitative"}
                                        >
                                        {this.getDataStack()}
                    </VictoryStack>
                    </VictoryChart>
                 </div>
              </Panel>
          </div>
      );
   }
}
