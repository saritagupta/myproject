import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import uuid from 'node-uuid';
import {Panel} from 'react-bootstrap';
class PanelComp extends React.Component {
    constructor(props) {
       super(props);
    }
     getLabel(){
         return <h3>{this.props.header}</h3>;
     }
    render(){
        return <Panel header={this.getLabel()} >
            {this.props.children}
        </Panel>;
    }

}
PanelComp.propTypes = {
      header: PropTypes.string
}

export default PanelComp;
