import Panel from './panel';
//import Panel from "./../../../app/Common/Components/Panel/panel";
const {
  renderIntoDocument,
  findRenderedDOMComponentWithTag,
  Simulate
} = TestUtils;

const mockFunction = () => {};
const props = {
  header: 'Welcome'
}

describe('Panel: Panel Element Component', () => {

  it('should take Panel to override default properties', () => {
    const component = renderIntoDocument(
      <Panel {...props}  />
    );
    expect(component.props.header).to.equal(props.header);
  });
});
