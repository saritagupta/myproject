import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { Navbar, Nav, NavItem, NavDropdown,MenuItem } from 'react-bootstrap';
import SessionStorage from './../../Utils/session-storage';
class Header extends React.Component {
    getLogin() {
     this.props.navigate('Login');
    };
    getRegistration() {
      this.props.navigate('Register');
    };
    getHome() {
       this.props.navigate('');
    };
    logout(){
       SessionStorage.clear();
       this.props.navigate('Login');
    }
    dataList(){
       this.props.navigate('ListData');
    }

    buildMenu(){
        return this.props.loggedIn ?
        <Nav pullRight >
            <NavItem eventKey={1}  onClick={this.dataList.bind(this)} >Data List</NavItem>
            <NavItem eventKey={1}  onClick={this.getHome.bind(this)} >Dashboard</NavItem>
            <NavItem eventKey={1}  onClick={this.logout.bind(this)} >Logout</NavItem>
        </Nav> :
        <Nav pullRight >
          <NavItem eventKey={2} onClick={this.getRegistration.bind(this)}>Register</NavItem>
          <NavItem eventKey={1}  onClick={this.getLogin.bind(this)} >Login</NavItem>
        </Nav>;
    }
   render() {

      return (
          <Navbar inverse>
              <Navbar.Header>
                <Navbar.Brand>
                  <a onClick={this.getHome.bind(this)}>HCL</a>
                </Navbar.Brand>
              </Navbar.Header>
              {this.buildMenu()}
            </Navbar>
      );
   }
}

export default Header;
