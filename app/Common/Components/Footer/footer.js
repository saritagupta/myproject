import React, { Component, PropTypes } from 'react';
export default class ApplicationFooter extends Component {
    render() {
        return (
            <footer className="footer">
                <div className="container text-center">
                  <p className="text-muted">&copy; HCL Technology</p>
                </div>
              </footer>
        );
    }

}
