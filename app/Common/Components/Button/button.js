import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
 class Button extends Component {
    onClick(e, props) {
        if (this.props.onClick) {
            this.props.onClick(e, props);
        }
    }

    _renderChildren() {
        if (this.props.processing) {
            return (
                <i className='fa fa-circle-o-notch fa-spin'></i>
            );
        }
        else if (this.props.icon) {
            const className = classNames('fa', this.props.icon);
            return (
                <i className={ className }></i>
            );
        }
        else {
            return this.props.children
        }
    }

    render() {
        const className = classNames('btn', this.props.className);
        return (
            <button id={this.props.id}
                    type={this.props.type || (this.props.datadismiss ? 'button' : null)}
                    className={className}
                    onClick={(e) => this.onClick(e, this.props)}
                    disabled={this.props.disabled}
                    data-toggle={this.props.datatoggle}
                    data-target={this.props.datatarget}
                    data-dismiss={this.props.datadismiss}
                    data-modal={this.props.datamodal}>
                {this._renderChildren()}
            </button>);
    }
}

Button.propTypes = {
    id: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    icon: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.arrayOf(PropTypes.element)]),
    processing: PropTypes.bool,
    datatoggle: PropTypes.string,
    datatarget: PropTypes.string,
    datadismiss: PropTypes.string,
    datamodal: PropTypes.string
};
export default Button;
