import Button from './button';
//import Panel from "./../../../app/Common/Components/Panel/panel";
const {
  renderIntoDocument,
  findRenderedDOMComponentWithTag,
  Simulate
} = TestUtils;

const mockFunction = () => {};
const props = {
  id: 'button',
  className:"btn-primary"
}

describe('Button: Button Element Component', () => {

  it('should take Button to override default properties', () => {
    const component = renderIntoDocument(
      <Button {...props}  />
    );
    expect(component.props.id).to.equal(props.id);
    expect(component.props.className).to.equal(props.className);
  });
});
