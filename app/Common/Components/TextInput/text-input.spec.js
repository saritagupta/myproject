import TextInput from './text-input';
//import Panel from "./../../../app/Common/Components/Panel/panel";
const {
  renderIntoDocument,
  findRenderedDOMComponentWithTag,
  Simulate
} = TestUtils;

const mockFunction = () => {};
const props = {
  placeholder: 'Welcome',
  name: 'Welcome',
  id: '1',
  onBlur: sinon.spy()
}

describe('TextInput: TextInput Element Component', () => {

  it('should take input to override default properties', () => {
    const component = renderIntoDocument(
      <TextInput {...props}  />
    );
    expect(component.props.id).to.equal(props.id);
    expect(component.props.name).to.equal(props.name);
    expect(component.props.placeholder).to.equal(props.placeholder);
  });
});
