import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import classNames from 'classnames';
import uuid from 'node-uuid';
import { FormGroup, ControlLabel,InputGroup,FormControl} from 'react-bootstrap';
class TextInput extends React.Component {
    constructor(props) {
       super(props);
       this.state={"validationState":''};
    }

    getLabel(){
         return this.props.label ?  <ControlLabel>{this.props.label}</ControlLabel> : "";
     }
     getInputGroup(){
         return <InputGroup>
           <InputGroup.Addon>{this.props.addon}</InputGroup.Addon>
           {this.getFormControl()}
         </InputGroup>;
     }
     getFormControl(){
        let {name,onBlur,placeholder,onChange} =this.props;
         return <input type="text"
                             placeholder={placeholder}
                             className="form-control"
                             ref='input'
                             name={name}
                             onBlur={onBlur}
                             placeholder={placeholder}
                            />;
     }
     buildFormControl(){
         return  this.props.addon ? this. getInputGroup(): this.getFormControl();
     }
     setFeedBack(){
         return this.props.feedback ? <FormControl.Feedback /> : "";
     }

    render(){
        let {formGroupId,bsSize,validationState} = this.props;
        let validateError =  this.state.validationState ? this.state : {};
        return <FormGroup controlId={formGroupId} bsSize={bsSize}  {...validateError} >
          {this.getLabel()}
          {this.buildFormControl()}
          {this.setFeedBack()}
        </FormGroup>;
    }
    getValue(){
        return this.refs.input.value;
    }

    setValue(value) {
        this.refs.input.value = value;
    }

    setError(errorMessage) {
        this.setState({"validationState":errorMessage});
    }

    getValidity() {
        return this.refs.input.validity;
    }
}
TextInput.propTypes = {
      field: PropTypes.object,
      name: PropTypes.string.isRequired,
      onBlur: PropTypes.func,
      onChangeFn:PropTypes.func

}
TextInput.getDefaultProps = {
         formGroupId :"formGroupId"+uuid.v1(),
         bsSize : "",
         validationState :"",
         formType: "text",
         addon:false
 };
export default TextInput;
