export default class LocalStorage {
    set(key, value) {
        if (LocalStorage.isAvailable) {
            window.localStorage.setItem(key, value);
        }
        return this;
    }

    get(key) {
        return LocalStorage.isAvailable && window.localStorage.getItem(key);
    }

    remove(key) {
        if (LocalStorage.isAvailable) {
            window.localStorage.removeItem(key);
        }
        return this;
    }
}
