 class SessionStorage {

}
SessionStorage.isAvailable = (typeof window !== 'undefined') && window.sessionStorage;
SessionStorage.set=(key, value)=> {
    if (SessionStorage.isAvailable) {
        window.sessionStorage.setItem(key, value);
    }
    return this;
}

SessionStorage.get =(key) => {
    return SessionStorage.isAvailable && window.sessionStorage.getItem(key);
}

SessionStorage.remove  = (key)=>{
    if (SessionStorage.isAvailable) {
        window.sessionStorage.removeItem(key);
    }
    return this;
}

SessionStorage.clear =() => {
    if (SessionStorage.isAvailable) {
        window.sessionStorage.clear();
    }
    return this;
}
export default SessionStorage;
