import { getUrlFor } from '../../config';
import fetch from 'isomorphic-fetch';
import _ from 'lodash';

export const POST = 'POST';
export const GET = 'GET';

function _wrapRequest(method, body) {
    var params = {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
    };

    if (!_.isEmpty(body) && (method === 'POST' || method === 'PATCH')) {
        params.body = !_.isString(body) && (params.headers['Content-Type'] === 'application/json') ? JSON.stringify(body) : body;
    }
    return params;
}

function _attachQueryParameters(url, method, body) {
    if (!_.isEmpty(body) && method === 'GET') {
        var newUrl = url.concat('?');
        var paramArray = [];
        for (var key in body) {
            if (Array.isArray(body[key])) {
                body[key].forEach((element, index) => {
                    for (var subKey in element) {
                        paramArray.push(encodeURIComponent(key) +
                        '[' + encodeURIComponent(index) + ']' +
                        '[' + encodeURIComponent(subKey) + ']=' +
                        encodeURIComponent(element[subKey]));
                    }
                })
            } else {
                paramArray.push(encodeURIComponent(key) + '=' + encodeURIComponent(body[key]));
            }
        }
        return newUrl.concat(paramArray.join('&'));
    }
    return url;
}

function _validateJSONResponse(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}

function _validateApplicationResults(response, serviceUrl) {
    if (!response) { throw Error('json response is null'); }
    const results = response.ApplicationResults;
    if (!results) { throw Error('ApplicationResults is null'); }
    if (!results.Success) {
        const error = new Error('ApplicationResults.Success=false');
        error.response = response;
        error.serviceUrl = serviceUrl;
        throw error;
    }
    return response;
}

export function getUrl(routeName) {
    return getUrlFor(routeName);
}


export function fetchJSON(url, method, body, dispatch) {
    const request = _wrapRequest(method, body);
    const requestUrl = _attachQueryParameters(url, method, body);
    return fetch(requestUrl, request)
        .then(response => _validateJSONResponse(response))
        .then(response => response.json())
        .catch((e) => {
            return new Promise(() => {});
        });
}
