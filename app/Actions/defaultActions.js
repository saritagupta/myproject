import { pushState } from 'redux-router';
export const CLEAR_STORE = 'CLEAR_STORE';
export const navigate = (route) => pushState(null, `/${route}`)
export const clearStore = () => {return { type: CLEAR_STORE } }
