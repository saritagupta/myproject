import * as DefaultActions from './defaultActions';
import * as SessionActions from './sessionActions';
import _ from 'lodash';

export function getActionCreators(...featureActions) {
    return _.assign({}, DefaultActions , SessionActions, ...featureActions);
}
