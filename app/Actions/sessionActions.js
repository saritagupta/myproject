import * as sessionService from './../Service/SessionService';
export const SESSION_SIGN_IN_REQUESTED = 'SESSION_SIGN_IN_REQUESTED';
export const SESSION_SIGN_IN_RESPONDED = 'SESSION_SIGN_IN_RESPONDED';
export const SET_UNAUTHORIZED = 'SET_UNAUTHORIZED';

export function signIn(credential) {
    return (dispatch) => {
        dispatch({
            type: SESSION_SIGN_IN_REQUESTED
        });
        return sessionService.signIn(credential.username, credential.password, dispatch)
            .then((response) => dispatch({
                    type: SESSION_SIGN_IN_RESPONDED,
                    user: response
                })
            );
  };
}
export function setUnauthorized() {
    return { type: SET_UNAUTHORIZED };
};
