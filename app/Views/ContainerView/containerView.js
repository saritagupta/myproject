import React, { Component, PropTypes } from 'react';

 class ContainerView extends Component {
    render() {
        return (
            <div className='container-fluid'>
                {this.props.children}
            </div>
          );
      }
}
ContainerView.propTypes = {
    children: PropTypes.node
}
export default ContainerView;
