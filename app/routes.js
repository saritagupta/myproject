import React from 'react';
import { Redirect, Router, Route, Link, browserHistory, IndexRoute  } from 'react-router';
import { ReduxRouter } from 'redux-router'
import App from './app';
import { ContainerView } from './Views';
import Home from './Components/Home/home';
import Register from './Components/Register/register';
import Login from './Components/Login/login';
import ListData from './Components/List/list';
export const getRoutes = function() {
    return (
        <ReduxRouter>
            <Route component={App}>
                <Route path='/' component={ContainerView}>
                    <IndexRoute component={Home} />
                    <Route path = "Home" component = {Home} />
                    <Route path = "Register" component = {Register} />
                    <Route path = "Login" component = {Login} />
                    <Route path = "ListData" component = {ListData} />                    
                </Route>
            </Route>
        </ReduxRouter>
    );
}
