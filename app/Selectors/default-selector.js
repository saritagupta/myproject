import { isAuthenticated } from './../Service/SessionService';
import _ from 'lodash';

export function select(state) {
     return {
         router: state.router,
         isAuthenticated: isAuthenticated()
     };
}
