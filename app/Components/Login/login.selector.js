import _ from 'lodash';
export function select(state) {
    return _.assign({},  {
        loginFormValue: _.result(state.session.get('loginForm'), 'toJS', {}),
        isSubmitted : state.session.get('isSubmitted'),
        isProcessing: state.session.get('isSigningIn'),
        isValid: state.session.get('isAuthenticated'),
        messages: _.result(state.session.get('messages'), 'toJS', state.session.get('messages')) || []
	});
}
