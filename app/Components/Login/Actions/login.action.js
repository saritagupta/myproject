import * as sessionService from '../../../Service/SessionService';
export const SET_LOGIN_REQUESTED = 'SET_LOGIN_REQUESTED';
export const LOGINFORM_SUBMITTED = 'LOGINFORM_SUBMITTED';
export const LOGINFORM_RESPONSE = 'LOGINFORM_RESPONSE';
export function signIn(credential) {
    return (dispatch) => {
        dispatch({
            type: LOGINFORM_SUBMITTED
        });
        return sessionService.signIn(credential.username, credential.password, dispatch)
            .then((response) => {
                dispatch({
                    type: LOGINFORM_RESPONSE,
                    user: response
                })}
            );
  };
}
