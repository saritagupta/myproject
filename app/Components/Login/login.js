import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { select } from './login.selector';
import { getActionCreators } from './../../Actions/action-creator';
import * as loginActions from './Actions/login.action';
import { Panel,FormGroup} from 'react-bootstrap';
import TextInput from './../../Common/Components/TextInput/text-input';
import Button from './../../Common/Components/Button/button';

@connect(select, (dispatch) => ({actions: bindActionCreators(getActionCreators(loginActions), dispatch)}))
export default class Login extends React.Component {
    constructor(props) {
        super(props);
    }
    ComponentDidMount(){
        //_setFormField();
    }
    _setFormField(){
      //let formVal={};
    }
    _onFormSubmit(e){
       e.preventDefault();

       if (_.isEmpty(_.trim(this.usernameInput.getValue()))) {
             this.usernameInput.refs.input.focus();
             this.usernameInput.setError("error");
         } else if (_.isEmpty(_.trim(this.passwordInput.getValue()))) {
             this.passwordInput.refs.input.focus();
             this.usernameInput.setError("");
             this.passwordInput.setError("error");
         } else {
             let userData={password:this.passwordInput.getValue(),username:this.usernameInput.getValue()};
             this.props.actions.signIn(userData).then(() => {
                if (this.props.isValid) {
                    this.props.actions.navigate('Home');
                }
            });
         }
    }
   render() {

        let getPanelHeader= () => {
            return <h3>Login</h3>;
        }

        let loginPanelHeader=<h3>Login</h3>;
            return (
                <div className="row">
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-6 col-sm-offset-3">
                                <Panel header={loginPanelHeader} >
                                <form noValidate  onSubmit={(e) => this._onFormSubmit(e)}>

                                    <TextInput
                                        ref={(element) => this.usernameInput = element}
                                        id="UserName"
                                        label="User Name"
                                        name="username"
                                        placeholder="username"

                                        />
                                    <TextInput
                                        ref={(element) => this.passwordInput = element}
                                        id="password"
                                        label="Password"
                                        name="password"
                                        placeholder="password"
                                        />
                                    <Button type='submit'
                                        className='btn btn-md btn-primary'
                                        disabled={false}
                                        processing={false}
                                        onClick={this._onFormSubmit.bind(this)}
                                        >
                                        Submit
                                    </Button>
                                    </form>
                                </Panel>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}
