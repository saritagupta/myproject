import { createReducer } from './../../Reducers/create-reducer';
import { SET_LOGIN_REQUESTED, LOGINFORM_SUBMITTED, LOGINFORM_RESPONSE} from './Actions/login.action';
import Immutable from 'immutable';
export const session = createReducer(Immutable.Map(), {
    [SET_LOGIN_REQUESTED](state,action) {
        return state.set('isLoading', action.val);
    },
    [LOGINFORM_SUBMITTED](state,action) {
        return state.merge({
            LoginResponse: null,
            isSubmitted: true
        });
    },
    [LOGINFORM_RESPONSE](state,action) {
        const user = action.user;
        return state.merge({
            agentName: user.UserName,
            isAuthenticated: user.LoginResponse.Success,
            ipAddress: user.IpAddress,
            messages: user.LoginResponse.ErrorMessages,
            isSubmitted: false
        });
    }
});
