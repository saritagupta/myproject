import { createReducer } from './../../Reducers/create-reducer';
import { DATALIST_REQUEST,
     DATALIST_RESPONSE} from './Actions/list.action';
import Immutable from 'immutable';
export const ListData = createReducer(Immutable.Map(), {

    [DATALIST_REQUEST](state,action) {
        return state.set('dataRequested', true);
    },
    [DATALIST_RESPONSE](state,action) {
        return state.merge({
            dataResponse: action.reponse,
            dataRequested: false
        });
    }
});
