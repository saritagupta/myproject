import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { select } from './list.selector';
import { getActionCreators } from './../../Actions/action-creator';
import * as ListActions from './Actions/list.action';
import { Panel,FormGroup, Table} from 'react-bootstrap';
//@connect(select, (dispatch) => ({actions: bindActionCreators(getActionCreators(homeActions), dispatch)}))

class ListComp extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount(){
        this.props.actions.getListData();
    }
    getRowdata(){
        return _.map(this.props.dataResponse,(val,index)=>{
            return  <tr key={index}>
                        <td>{index+1}</td>
                        <td>{val.Name}</td>
                        <td>{val.Age}</td>
                        <td>{val.Access}</td>
                        <td>{val.Email}</td>
                      </tr>;
        });
    }
   render() {
       console.log("dataResponse",this.props.dataResponse);
      return (
         <div>
             <div className="row">
                <div className="col-sm-12">

                        <Table striped bordered condensed hover>

                            <thead>
                              <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Age</th>
                                <th>Access</th>
                                <th>Email</th>
                              </tr>
                            </thead>
                            <tbody>
                            {this.getRowdata()}
                            </tbody>
                        </Table>

                </div>

            </div>
        </div>
      );
   }
}
//export default Home;
export default connect(select,  (dispatch) => ({actions: bindActionCreators(getActionCreators(ListActions), dispatch)}))(ListComp);
