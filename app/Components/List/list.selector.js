import _ from 'lodash';
export function select(state) {
    return _.assign({},  {
        dataResponse : _.result(state.ListData.get('dataResponse'), 'toJS', {}),
        dataRequested : _.result(state.ListData.get('dataRequested'), 'toJS',[])
	});
}
