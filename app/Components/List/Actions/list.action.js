import * as reportService from '../../../Service/report-service';
export const DATALIST_REQUEST = 'DATALIST_REQUEST';
export const DATALIST_RESPONSE = 'DATALIST_RESPONSE';
export function getListData(val){
    return (dispatch) => {
         dispatch({
            type:'DATALIST_REQUEST'
        });
        return reportService.getListData(val,dispatch)
        .then((reponse)=>{
            dispatch({
                type:'DATALIST_RESPONSE',
                reponse
            });
        });
    }
}
