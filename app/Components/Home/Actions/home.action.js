import * as reportService from '../../../Service/report-service';
export const SET_TEMP_REQUESTED = 'SET_TEMP_REQUESTED';
export const REPORT_RESPONSE = 'REPORT_RESPONSE';
export const REPORT_REQUEST = 'REPORT_REQUEST';
export const REPORTPIE_REQUEST = 'REPORTPIE_REQUEST';
export const REPORTPIE_RESPONSE = 'REPORTPIE_RESPONSE';
export function setTemp(setTemp) {
    return { type: SET_TEMP_REQUESTED, setTemp };
};

export function getpieReportData(val){
    return (dispatch) => {
         dispatch({
            type:'REPORTPIE_REQUEST'
        });
        return reportService.getPieReport(val,dispatch)
        .then((reponse)=>{
            dispatch({
                type:'REPORTPIE_RESPONSE',
                reponse
            });
        });
    }
}
export function getLineReportData(val){
    return (dispatch) => {
         dispatch({
            type:'REPORT_REQUEST'
        });
        return reportService.getLineReport(val,dispatch)
        .then((reponse)=>{
            dispatch({
                type:'REPORT_RESPONSE',
                reponse
            });
        });
    }
}
