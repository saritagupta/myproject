import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { select } from './home.selector';
import { getActionCreators } from './../../Actions/action-creator';
import * as homeActions from './Actions/home.action';
import ChartVicPie from './../../Common/Components/Chart/chartnew';
import ChartVicBar from './../../Common/Components/Chart/chartVbar';
import { Panel,FormGroup} from 'react-bootstrap';
class Home extends React.Component {
    constructor(props) {
        super(props);
    }
    componentWillMount(){
        this.props.actions.getLineReportData();
        this.props.actions.getpieReportData();
    }
   render() {
      return (
         <div>
             <div className="row">
                <div className="col-sm-6">
                    <ChartVicPie innerRadius={true} data={this.props.reportPieResponse.data} />
                </div>
                <div className="col-sm-6">
                     <ChartVicBar {...this.props.reportResponse}/>
                </div>
            </div>
            <div className="row">
                <div className="col-sm-6">
                    <ChartVicBar Data ={this.props.reportResponse.Data1} labels ={this.props.reportResponse.labels1} />
                </div>
                <div className="col-sm-6">

                    <ChartVicPie  data={this.props.reportPieResponse.data1} />
                </div>
            </div>
        </div>
      );
   }
}
//export default Home;
export default connect(select,  (dispatch) => ({actions: bindActionCreators(getActionCreators(homeActions), dispatch)}))(Home);
