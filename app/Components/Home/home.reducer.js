import { createReducer } from './../../Reducers/create-reducer';
import { SET_TEMP_REQUESTED,
     REPORT_REQUEST,
     REPORT_RESPONSE,
     REPORTPIE_RESPONSE,
     REPORTPIE_REQUEST} from './Actions/home.action';
import Immutable from 'immutable';
export const home = createReducer(Immutable.Map(), {
    [SET_TEMP_REQUESTED](state,action) {
        return state.set('isLoadingHome', action.setTemp);
    },
    [REPORT_REQUEST](state,action) {
        return state.set('reportRequested', true);
    },
    [REPORT_RESPONSE](state,action) {
        return state.merge({
            reportResponse: action.reponse,
            reportRequested: false
        });
    },
    [REPORTPIE_REQUEST](state,action) {
        return state.set('reportPieRequested', true);
    },
    [REPORTPIE_RESPONSE](state,action) {
        return state.merge({
            reportPieResponse: action.reponse,
            reportPieRequested: false
        });
    }
});
