import expect from 'expect';
import Immutable from 'immutable';
import * as actions from './Actions/home.action';
import { home as reducer } from './home.reducer';

describe('reducers', () => {

    it('should handle REPORT_REQUEST', () => {
        expect(
            reducer(Immutable.Map(), {
                type: actions.REPORT_REQUEST
            })).toEqual(Immutable.Map({reportRequested: true}));
    });
    it('should handle REPORTPIE_REQUEST', () => {
        expect(
            reducer(Immutable.Map(), {
                type: actions.REPORTPIE_REQUEST
            })).toEqual(Immutable.Map({reportPieRequested: true}));
    });

})
