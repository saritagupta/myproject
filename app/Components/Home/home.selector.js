import _ from 'lodash';
export function select(state) {
    return _.assign({},  {
        isLoadingHome: state.home.get('isLoadingHome'),
        reportResponse : _.result(state.home.get('reportResponse'), 'toJS', {}),
        reportPieResponse : _.result(state.home.get('reportPieResponse'), 'toJS',[])
	});
}
