import { createReducer } from './../../Reducers/create-reducer';
import { SET_REGISTER_REQUESTED, REGISTERFORM_SUBMITTED, REGISTERFORM_RESPONSE} from './Actions/register.action';
import Immutable from 'immutable';
export const register = createReducer(Immutable.Map(), {
    [SET_REGISTER_REQUESTED](state,action) {
        return state.set('isLoadingHome', action.val);
    },
    [REGISTERFORM_SUBMITTED](state,action) {
        return state.merge({
            registrationResponse: null,
            isSubmitted: true
        });
    },
    [REGISTERFORM_RESPONSE](state,action) {
        return state.merge({
            registrationResponse: action.reponse,
            isSubmitted: false
        });
    }

});
