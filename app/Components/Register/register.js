import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { select } from './register.selector';
import { getActionCreators } from './../../Actions/action-creator';
import * as registrationActions from './Actions/register.action';
import { Panel,FormGroup} from 'react-bootstrap';
import TextInput from './../../Common/Components/TextInput/text-input';
import Button from './../../Common/Components/Button/button';
class Registration extends React.Component {
    constructor(props) {
        super(props);
    }
    ComponentDidMount(){
        //_setFormField();
    }
    _setFormField(){
      //let formVal={};
    }
    _onFormSubmit(e){
       e.preventDefault();
       if (_.isEmpty(_.trim(this.usernameInput.getValue()))) {
             this.usernameInput.refs.input.focus();
             this.usernameInput.setError("error");
         } else if (_.isEmpty(_.trim(this.passwordInput.getValue()))) {
             this.passwordInput.refs.input.focus();
             this.usernameInput.setError("");
             this.passwordInput.setError("error");
         } else {
             let userData={password:this.passwordInput.getValue(),username:this.usernameInput.getValue()};
             this.props.actions.register(userData).then(() => {
                if (this.props.registrationResponse) {
                 this.ClearFormValue()
                }
            });
         }
    }
    ClearFormValue(){
         this.usernameInput.refs.input.value ="";
         this.passwordInput.refs.input.value = '';
    }

    displayResponse(){
        let panelHeader=<h3>Thank You</h3>;
          return !_.isEmpty(this.props.registrationResponse)  ?
        <Panel header={panelHeader} bsStyle="primary"  >
            Thanks you for Registering with us<br/><br/>

            Please Activate Your Account. Before you can login, you must active your account with the code sent to your email address.
            If you did not receive this email, please check your junk/spam folder.<br/><br/>
        </Panel>: "";
    }
   render() {
        let RegistationPanelHeader=<h3>Register</h3>;
            return (
                <div className="row">
                    <div className="col-sm-12">
                        <div className="row">
                            <div className="col-sm-6">
                                <Panel header={RegistationPanelHeader}  >
                                <form noValidate  onSubmit={(e) => this._onFormSubmit(e)}>

                                    <TextInput
                                        ref={(element) => this.usernameInput = element}
                                        id="UserName"
                                        label="User Name"
                                        name="username"
                                        placeholder="username"

                                        />
                                    <TextInput
                                        ref={(element) => this.passwordInput = element}
                                        id="password"
                                        label="Password:"
                                        name="password"
                                        placeholder="password"
                                        />
                                    <Button type='submit'
                                        className='btn btn-md btn-primary'
                                        disabled={false}
                                        processing={false}
                                        onClick={this._onFormSubmit.bind(this)}
                                        >
                                        {this.props.isSubmitted && !this.props.registrationResponse ? "Loading": "Register Now"}
                                    </Button>
                                    </form>

                                </Panel>

                            </div>
                            <div className="col-sm-6">
                                {this.displayResponse()}
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}
export default connect(select,  (dispatch) => ({actions: bindActionCreators(getActionCreators(registrationActions), dispatch)}))(Registration);
