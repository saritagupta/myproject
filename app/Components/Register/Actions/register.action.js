import * as registerService from '../../../Service/register-service';
export const SET_REGISTER_REQUESTED = 'SET_REGISTER_REQUESTED';
export const REGISTERFORM_RESPONSE = 'REGISTERFORM_RESPONSE';
export const REGISTERFORM_SUBMITTED = 'REGISTERFORM_SUBMITTED';
export function register(val){
    return (dispatch) => {
         dispatch({
            type:'REGISTERFORM_SUBMITTED'
        });
        return registerService.register(val,dispatch)
        .then((reponse)=>{
            dispatch({
                type:'REGISTERFORM_RESPONSE',
                reponse
            });
        });
    }
}
