import _ from 'lodash';
export function select(state) {
    return _.assign({},  {
        registerFormValue: _.result(state.register.get('registerForm'), 'toJS', {}),
        isSubmitted : state.register.get('isSubmitted'),
        registrationResponse:_.result(state.register.get('registrationResponse'), 'toJS', {})
	});
}
