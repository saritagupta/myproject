//const serviceUrl = API_URL;
const serviceUrl = 'http://localhost:8080/api';

const serverRoutes = {
    queryReport : '/report.json',
    listData: "/listData.json",
    lineChart: "/lineChart.json",
    pieChart: "/pieChart.json",
    register: '/registration.json',
    login:'/login.json'
};

export function getUrlFor(routeName) {
    return serviceUrl + serverRoutes[routeName];
}
